ilength::[a] -> Integer
ilength [] = 0
ilength (x : xs) = 1 + ilength (xs)

fib::Int->[Int]
fib 0 = [0]
fib 1 = [1, 0]
fib n = (head (fib (n-1)) + head (fib (n-2))) : fib (n-1)
