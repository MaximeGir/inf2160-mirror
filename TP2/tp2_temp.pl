  % pour charger predicats de liste
:- use_module(library(lists)).

link(max, jean).
link(max, claire).
link(jean, claire).
link(jean, zoe).
link(claire, anne).
link(claire, guy). 
link(claire, lise).
link(lise, guy).
link(anne, guy).
link(anne, roger).
link(roger, alex).

symlink(F1, F2) :- link(F1, F2).
symlink(F1, F2) :- link(F2, F1).

profile(max, jazz).
profile(jean, classique).
profile(claire, blues ).
profile(zoe, baroque).
profile(anne, rock).
profile(lise, country).
profile(guy, pop).
profile(roger, pop). 
profile(alex, jazz).

taglist([classique, baroque, jazz, blues, country, rock, pop]).


:- dynamic(friendlis/1).

friends(Friend, LL)  :- asserta(friendlis([])), friends2(Friend), retract(friendlis(LL)).
/*
friends2(F):- retract(friendlis(_LL)), findall(Y, symlink(F,Y), X), rev(X,P), asserta(friendlis(P)).

rev([],[]). 
rev([T|Q],R):- rev(Q,E), append(E,[T],R).
*/

friends2(F) :-  findall(X,symlink(F,X),L),reverse(L,L2),!, asserta(friendlis(L2)) , retract(friendlis([])).

/*
reverse([],Z,Z).

reverse([H|T],Z,Acc) :- reverse(T,Z,[H|Acc]).
*/

/*
length1(_,[],0).
length1(E,[T|Q],N):- E \= T, write(T),
                                       N is NQ +1,
                                       length1(E,Q,NQ),!.*/

pos(E, L, NP) :- pos2(E, L, 0, NP).


pos2(,[],0,).
pos2(E,[T|Qtaglist],Compte,NP) :- E = T,NP is Compte,!.
pos2(E, [T|Qtaglist], Compte, NP) :-pos2(E,Qtaglist,Compte,NP1),
                                                                       NP is NP1 + 1,!.

semdist(Tag1, Tag2, D) :- taglist(L),  pos(Tag1, L , N1), pos(Tag2, L , N2), D is abs(N1 - N2).

maxcount(5).

compat(Friend, Tag) :- profile(Friend,Profile), semdist(Profile,Tag,D), abs(D) =< 1.

%pick([],E).
pick([T|Q],T).
pick([T|Q],E) :- pick(Q,E).



test(A):- maxcount(L), A= L.


find(Start, Tag, Rpath):- find2(Start, Tag, 0, [], Rpath).

find2(Step, Tag, Count, Path, Rpath):- 
                       maxcount(T), not(Count > T), 
                       not(member(Step, Path)), 
                       friends(Step, FL),write(FL),
                       pick(FL, Ami),
                       profile(Ami, Pf), 
                       compat(Step, Pf), 
                       CC is Count+1, 
                       find2(Ami, Tag, CC, [Step, Ami | Path], Rpath). 
find2(Step, Tag, Count, Path, Path).