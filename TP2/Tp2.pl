% pour charger predicats de liste
:- use_module(library(lists)).

link(max, jean).
link(max, claire).
link(jean, claire).
link(jean, zoe).
link(claire, anne).
link(claire, guy). 
link(claire, lise).
link(lise, guy).
link(anne, guy).
link(anne, roger).
link(roger, alex).

symlink(F1, F2):- link(F1, F2).
symlink(F1, F2):- link(F2, F1).

profile(max, jazz).
profile(jean, classique).
profile(claire, blues ).
profile(zoe, baroque).
profile(anne, rock).
profile(lise, country).
profile(guy, pop).
profile(roger, pop). 
profile(alex, jazz).

taglist([classique, baroque, jazz, blues, country, rock, pop]).

:- dynamic(friendlis/1).

friends(Friend, LL):- asserta(friendlis([])), friends2(Friend), retract(friendlis(LL)).

friends2(F):- retract(friendlis(_LL)), findall(Y, symlink(F,Y), X), rev(X,P), asserta(friendlis(P)).

pos(E, L, Res):- pos2(E,L,0,Res).
pos2(E, [E|_], N, N):- !.
pos2(E, [ _ |Queue], N, R):- NN is N+1, pos2(E, Queue, NN, R).

semdist(Tag1, Tag2, D):- taglist(L), pos(Tag1, L, N), pos(Tag2, L, NN), D is abs(NN - N).

maxcount(5).

not(But) :- call(But),!,fail.
not(But).

compat(Friend, Tag) :- profile(Friend, R), semdist(Tag, R, N), N =< 1.

pick([T|_],T).
pick([T|Q],E):- pick(Q,E).

find(Start, Tag, Rpath):- find2(Start, Tag, 0, [], Rpath).
find2(Step, Tag, Count, Path, Rpath):- 
       compat(Step,Tag),
       append([Step],Path,Rpath).
find2(Step, Tag, Count, Path, Rpath):- 
       maxcount(T), Count =< T, 
       friends(Step, FL),
       pick(FL, Ami),
       profile(Ami, Pf), 
       not(member(Ami, Path)), 
       CC is Count+1, 
       find2(Ami, Tag, CC, [Step | Path], Rpath).  
find2(_Step, _Tag, _Count, Path, Path).







