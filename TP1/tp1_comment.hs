-- Girard / Maxime / GIRM30058500
--
  
-- Définition de type utile.
-- Link , une relation d'amitié.
-- Friend , une chaine de char correspondant au prénom de l'individu constituant un ami.
-- Graph , une liste de Link , donc une liste de relation d'amitié.

type Link = (Friend, Friend)
type Friend = String
type Graph = [Link]


-- Peupler Graph d'élément de type Link , affecté à la constante socnet

socnet :: Graph
socnet = 
    [("max", "jean"), 
    ("max", "claire"), 
    ("jean", "claire"), 
    ("jean", "zoe"), 
    ("claire", "anne"), 
    ("claire", "guy"), 
    ("claire", "lise"), 
    ("lise", "guy"), 
    ("anne", "guy"), 
    ("anne", "roger"), 
    ("roger", "alex")]


-- Les relation d'amitié (dans ce programme) sont bilatérale, nous inversons
-- donc les Friend de chaque Link du Graph, et l'affectons à la constante symnet.
-- On appelle la fonction inverlink pour faire cette manipulation.

symnet :: Graph
symnet = socnet ++ (map inverlink socnet)

inverlink :: Link -> Link
inverlink (f1, f2) = (f2, f1)


-- Fonctions startLink et endLink, utiles à la fonction Friends , qui permet de localiser
-- facilement les éléments nécéssaire à la construction d'une liste d'ami.
--On appelle la fonction friends pour generer une liste d'ami.

startLink :: Link -> Friend
startLink (start, end ) = start

endLink :: Link -> Friend
endLink ( start , end) = end

friends :: Friend -> [Friend]
friends friend = [endLink x | x<-symnet , (startLink x == friend)]


-- Déclaration des nouveaux types Tag, composé d'une chaine de caractère,
-- et Profile, qui réutilise notre définition de Friend et la jumelle (en tuplet)
-- à Tag.

type Tag = String
type Profile = (Friend, Tag)


-- Fonction friendProf retourne le nom de l'ami auquel correspond le Profile passé en argument.

friendProf :: Profile -> Friend
friendProf (ami , etiquette ) = ami 


-- Fonction tagProf retourne le Tag de l'ami auquel correspond le Profile passé en argument.

tagProf :: Profile -> Tag
tagProf ( ami , etiquette ) = etiquette 


-- Affection d'une liste de Profile à la constante profiles.

profiles :: [Profile]
profiles = [("max", "jazz"), ("jean", "classique"), ("claire", "blues"), ("zoe", "baroque"), ("anne", "rock"), ("lise", "country"), ("guy", "pop"), ("roger", "pop"), ("alex", "jazz")]


-- Fonction getprofile retourne un Tag à partir d'un élément Friend , utilse la fonction getprofx.

getprofile :: Friend -> Tag
getprofile friend = getprofx friend profiles


-- Fonction getprofx utilisé dans la récuperation promise du Tag associé à un Friend par getprofile.

getprofx :: Friend -> [Profile] -> Tag
getprofx _ [] = error "profile not found"
getprofx friend (x : xs) = if friend == friendProf x then tagProf x
                           else getprofx friend xs


-- Affection d'une constante : une liste de Tag

taglist :: [Tag]
taglist = ["classique", "baroque", "jazz", "blues", "country", "rock", "pop"]


-- Fonction pos qui retrouve la position [0 ,...[ d'un Tag dans l'échelle sémantique.

pos :: (Eq a) => a -> [a] -> Int
pos e list = posx e list 0


-- Fonction qui utilise pos pour détermier le rang de la position demandé dans la function pos.

posx :: (Eq a) => a -> [a] -> Int -> Int
posx r [] n = error "element not found"
posx element (x:xs) n = if element == x 
                  then n 
                  else posx element xs n+1


-- Fonction semdist qui retourne la distance sémantique entre deux Tag.

semdist :: Tag -> Tag -> Int
semdist tag1 tag2 = abs((pos tag1 taglist) - (pos tag2 taglist))


-- Déclaration de la constante maxcount, sert à la recherche en profondeur, nombre maximal
-- de lien à examiner lors de la fouille utilisé dans la fonction find (voir la fonction find).

maxcount :: Int
maxcount = 5


-- Définition du type Path composé d'une liste de Friend, utile dans la fonction find et qui 
-- correspond à la liste d'ami composé lors de la recherche initialisé par la fonction find.

type Path = [Friend]


-- Fonction compat qui verifie si un Friend est associé directement à un Tag dans son Profile

compat :: Friend -> Tag -> Bool
compat friend tag =  tag == (getprofile friend)


-- Fonction nexts qui retourne une liste d'ami selon leur distance (en ordre croissant) sémantique 
-- par rapport au Tag passé en paramètre.
nexts :: Friend -> Tag -> [Friend]
nexts friend goal = insort [ x | x<-friends friend, y<-friends friend, if x == y then True else False] (\x y-> if semdist (getprofile x) goal < semdist (getprofile y) goal then True else False)  


-- Fonction de tri par insertion utile à la fonction nexts

insort :: [a] -> (a -> a -> Bool) -> [a]
insort [] f     = []
insort (x : xs) f   =   ins x (insort xs f)
                where
                ins x []       = [x]
                ins x (y : ys) = if (f x y) 
                                 then x : y : ys
                                 else y : ins x ys


-- Fonction find qui permet de retrouver un Path reliant le Friend et la recherche 
-- d'un Tag dans les Profile, autrement dit, c'est le Path à emprunter pour trouver la premiere
-- personne d'intérêt (dont son Profile correspond au Tag passé en paramètre).

find :: Friend -> Tag -> Path
find start goal = findx start goal 0 []


-- Fonction findx est utilisé par la fonction find et contient l'algoritme de recherche en profondeur
-- et évalue les chemins possibles.

findx :: Friend -> Tag -> Int -> Path -> Path
findx step goal count path | count > maxcount = [] 
                           | compat step goal = reverse (step:path)
	                         | otherwise = (loop (nexts step goal))
                           where loop::[Friend] -> Path
                                 loop [] = []
                                 loop (x:xs) = (findx x goal (count+1) (step:path))