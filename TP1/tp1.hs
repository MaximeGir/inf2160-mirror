-- Girard / Maxime / GIRM30058500
--

module Tp1 where

type Link = (Friend, Friend)
type Friend = String
type Graph = [Link]

socnet :: Graph
socnet = 
    [("max", "jean"), 
    ("max", "claire"), 
    ("jean", "claire"), 
    ("jean", "zoe"), 
    ("claire", "anne"), 
    ("claire", "guy"), 
    ("claire", "lise"), 
    ("lise", "guy"), 
    ("anne", "guy"), 
    ("anne", "roger"), 
    ("roger", "alex")]

symnet :: Graph
symnet = socnet ++ (map inverlink socnet)

inverlink :: Link -> Link
inverlink (f1, f2) = (f2, f1)

startLink :: Link -> Friend
startLink (start, end ) = start

endLink :: Link -> Friend
endLink ( start , end) = end

friends :: Friend -> [Friend]
friends friend = [endLink x | x<-symnet , (startLink x == friend)]

type Tag = String
type Profile = (Friend, Tag)

friendProf :: Profile -> Friend
friendProf (ami , etiquette ) = ami 

tagProf :: Profile -> Tag
tagProf ( ami , etiquette ) = etiquette 

profiles :: [Profile]
profiles = [("max", "jazz"), ("jean", "classique"), ("claire", "blues"), ("zoe", "baroque"), ("anne", "rock"), ("lise", "country"), ("guy", "pop"), ("roger", "pop"), ("alex", "jazz")]

getprofile :: Friend -> Tag
getprofile friend = getprofx friend profiles

getprofx :: Friend -> [Profile] -> Tag
getprofx x [] = error "profile not found"
getprofx friend (x : xs) = if friend == friendProf x then tagProf x
                           else getprofx friend xs

taglist :: [Tag]
taglist = ["classique", "baroque", "jazz", "blues", "country", "rock", "pop"]

pos :: (Eq a) => a -> [a] -> Int
pos e list = posx e list 0

posx :: (Eq a) => a -> [a] -> Int -> Int
posx r [] n = error "element not found"
posx element (x:xs) n = if element == x 
                  then n 
                  else posx element xs n+1

semdist :: Tag -> Tag -> Int
semdist tag1 tag2 = abs((pos tag1 taglist) - (pos tag2 taglist))

maxcount :: Int
maxcount = 5

type Path = [Friend]

compat :: Friend -> Tag -> Bool
compat friend tag =  tag == (getprofile friend)

nexts :: Friend -> Tag -> [Friend]
nexts friend goal = insort [ x | x<-friends friend, y<-friends friend, if x == y then True else False] (\x y-> if semdist (getprofile x) goal < semdist (getprofile y) goal then True else False)  

insort :: [a] -> (a -> a -> Bool) -> [a]
insort [] f     = []
insort (x : xs) f   =   ins x (insort xs f)
                where
                ins x []       = [x]
                ins x (y : ys) = if (f x y) 
                                 then x : y : ys
                                 else y : ins x ys

find :: Friend -> Tag -> Path
find start goal = findx start goal 0 []

findx :: Friend -> Tag -> Int -> Path -> Path
findx step goal count path | count > maxcount = [] 
                           | compat step goal = reverse (step:path)
	                   | otherwise = (loop (nexts step goal))
                           where loop::[Friend] -> Path
                                 loop [] = []
                                 loop (x:xs) = (findx x goal (count+1) (step:path))
